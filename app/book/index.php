
<?php

$base = '../../include/';
//including the database connection file
include_once $base ."classes/Crud.php";
 
$crud = new Crud();
 
//fetching data in descending order (lastest entry first)
$query = "SELECT * FROM books ORDER BY id DESC";
$result = $crud->getData($query);
//echo '<pre>'; print_r($result); exit;
?>

<?php include $base .'header.php';?> 
<body>
	<div class="container">
		<div class="row">
		    <div class="col-md-6">
		        <h4>Add the details of Books</h4>
		    </div>
		    <div class="col-md-6">
		        <button type="button" class="btn btn-primary" onClick="document.location.href='add.php'">ADD</button>
		    </div> 
		</div>
		<hr>
		<h4> List of Books: </h4><br/>
		<table>
			<tr>
		        <td>Name</td>
		        <td>Issued ID</td>
		        <td>Issued By</td>
		        <td>Action</td>
		    </tr>

		    <?php 
		    foreach ($result as $key => $res) {
		    //while($res = mysqli_fetch_array($result)) {         
		        echo "<tr>";
		        echo "<td>".$res['name']."</td>";
		        echo "<td>".$res['student_id']."</td>";
		        echo "<td>".$res['sname']."</td>";
		        echo "<td>
				        <a href=\"edit.php?id=$res[id]\"><span class='glyphicon glyphicon-pencil'></span></a>
				        <a href=\"delete.php?id=$res[id]\" onClick=\"return confirm('Are you sure you want to delete?')\"><span class='glyphicon glyphicon-trash'></span></a>
				     </td>";        
		    }
		    ?>
		</table>
	</div>
</body>

<?php include $base .'footer.php';?>