<?php 
$URL ='http://localhost/sakshioop/';
$BASE_URL='http://localhost/sakshioop/app/';
?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>School DBMS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <style type="text/css">
        header{
            color: rgb(219, 153, 66);
            text-align: center;
        }

        /* Add a black background color to the top navigation */
        .topnav {
            background-color: rgb(248, 248, 248);
            overflow: hidden;

            
          }
          
          /* Style the links inside the navigation bar */
          .topnav a {
            float: left;
            color: black;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
          }
          
          /* Change the color of links on hover */
          .topnav a:hover {
            background-color: #ddd;
            color: black;
          }
          
          /* Add a color to the active/current link */
          .topnav a.active {
            background-color: rgb(161, 161, 161);
            color: white;
          }

         .list{
             padding-top: 20px;
         }
         table, th,td {
            background-color: #ffffff;
            border: 1px solid gray;
            width: 100%;
            padding: 12px 20px;
            margin: 5px 0;
            

          }
          table{
            border-collapse: collapse;
          }
          th{
            color: #696969;
            border: 1px solid gray;
          }
          input[type=text], input[type=number],input[type=submit]{
            width: 100%;
            padding: 12px 20px;
            margin: 8px 0;
            display: inline-block;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
          }
         footer{
            background-color: rgb(248, 248, 248);
            text-align: center;
            /*text-decoration-color:#4169E1;*/
            font-size: 15px;
            margin-top: 40px;


        }

        .row{
          padding-top: 20px;
          }
        
    </style>
         

    </head>
    <body>
        <div class="container">
        <header>
            <h3 ><img src="include/image/schoolhouse.jpg" style="width:50px; height:45px;">School Database Management System</h3>
        </header>
        <div class="topnav">
                <a class="active" href="<?php echo $URL;?>index.php">School</a>
                <a href="<?php echo $BASE_URL;?>student/index.php">Student</a>
                <a href="<?php echo $BASE_URL;?>book/index.php">Book</a>
                <a href="#">Teacher</a>
                <a href="#">Department</a>
                <a href="#">Course</a>
        </div> 
        </div>

    </body>
    </html>
