<?php 
$base = '../../include/';

include_once $base ."classes/Crud.php";

$crud = new Crud();
$id = $crud->escape_string($_GET['id']);
$result = $crud->delete($id, 'books');

if($result) {
	header("Location:index.php");
}


?>