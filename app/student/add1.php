
<?php

$base = '../../include/';
include $base .'header.php';

?>
<body>
	<div class="container">
		<form method="post" action="add1.php" name="form1" >
		    <h2>Enter your details</h2> 
		        <table id="table">
                    <tr>
                        <td>
                            <label>Name</label> <input type="text" name="name">
		                    <label>Age</label> <input type="text" name="age">
		                    <label>Email</label> <input type="text" name="email">
		                    <label>Gender</label><select class="form-control" type="text" name="gender">
                                <option>Male</option>
		                        <option>Female</option>
		                        <option>Others</option>
                            </select>
                            <input type="submit" name="Submit" value="Add">
                        </td>
                    </tr>
                </table>
		</form>
	</div>
</body>

<?php 
include $base .'footer.php';
include_once $base ."classes/Crud.php";
include_once $base ."classes/Validation.php";
 
$crud = new Crud();
$validation = new Validation();
 
if(isset($_POST['Submit'])) {    
    $name = $crud->escape_string($_POST['name']);
    $age = $crud->escape_string($_POST['age']);
    $email = $crud->escape_string($_POST['email']);
    $gender = $crud->escape_string($_POST['gender']);
        
    $msg = $validation->check_empty($_POST, array('name', 'age', 'email','gender'));
    $check_age = $validation->is_age_valid($_POST['age']);
    $check_email = $validation->is_email_valid($_POST['email']);
    
    // checking empty fields
    if($msg != null) {
        echo $msg;        
        //link to the previous page
        echo "<br/><a href='javascript:self.history.back();'>Go Back</a>";
    } elseif (!$check_age) {
        echo 'Please provide proper age.';
    } elseif (!$check_email) {
        echo 'Please provide proper email.';
    }    
    else { 
    	//die('testing');
        // if all the fields are filled (not empty) 
            
        //insert data to database    
        $result = $crud->execute("INSERT INTO students (name,age,email,gender) VALUES ('$name','$age','$email','$gender')");
        header('Location:index.php');
        //display success message
        //echo "<font color='green'>Data added successfully.";
        //echo "<br/><a href='index.php'>View Result</a>";
    }

    }
    ?>

